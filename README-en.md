CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Server installation
 * Requirements
 * Installation
 * Configuration
 * Maintainer


INTRODUCTION
------------

Intends to show how to dialog with a JSON API server (drupal 8 in this particular case).

 * For a full description of the module, visit the project page:
   [https://www.drupal.org/project/jsonapi_example](https://www.drupal.org/project/jsonapi_example)

 * To submit bug reports and feature suggestions, or to track changes:
   [https://www.drupal.org/project/issues/search/jsonapi_example](https://www.drupal.org/project/issues/search/jsonapi_example)

SERVER INSTALLATION
-------------------

On the server, activate JSON API, and install simple_oauth, 

    composer require drupal/simple_oauth
    drush en -y simple_oauth
    drush en -y jsonapi
    
Goto admin/config/services/jsonapi and Accept all JSON API crud operations.  

Goto admin/config/people/simple_oauth  and generate keys.  

Create specific role and user for webservices, give global permissions for the targets content type.
  
Goto admin/config/services/consumer and create client with the new created account and role.  

REQUIREMENTS
------------

No special requirements.


THIS CLIENT INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.  See: [https://www.drupal.org/node/1897420](https://www.drupal.org/node/1897420)  for further information. 

    composer require drupal/jsonapi_example
    drush en -y jsonapi_example
    
* configure and use it at: '/examples/jsonapi-example'

CONFIGURATION
-------------

In top of the page, uncollapse the target server settings and fill-it .


MAINTAINER
-----------

Maintainer:
[Jerome LEGENDRE](https://www.drupal.org/u/jeromelegendre)
