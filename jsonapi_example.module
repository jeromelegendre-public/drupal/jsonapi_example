<?php

/**
 * @file
 * Module de demonstration d'echange avec un serveur JSON API.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityInterface;
use \Drupal\jsonapi_example;

const README_EN = 'README-en.md';

/**
 * Implements hook_help().
 */
function jsonapi_example_help($route_name, RouteMatchInterface $route_match) {

  if ('help.page.jsonapi_example' === $route_name) {
    $readme_content = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . README_EN);
    if (\Drupal::moduleHandler()->moduleExists('markdown')) {
      // Use the Markdown filter to render the README.
      $filter_manager = \Drupal::service('plugin.manager.filter');
      $markdown_config = \Drupal::configFactory()
        ->get('markdown.settings')
        ->getRawData();
      $markdown_settings = ['settings' => $markdown_config];
      /** @var \Drupal\markdown\Plugin\Filter\MarkdownFilterInterface $filter */
      $filter = $filter_manager->createInstance('markdown', $markdown_settings);
      return $filter->process($readme_content, 'en');
    }
    else {
      return [
        '#type' => 'html_tag',
        '#tag' => 'pre',
        '#value' => $readme_content,
      ];
    }
  }

}

/**
 * Implements hook_entity_insert().
 */
function jsonapi_example_entity_insert(EntityInterface $entity) {
  if ($entity->getEntityTypeId() == 'node' && $entity->bundle() == 'article') {
    $title = $entity->get('title')->value;
    $body = $entity->get('body')->value;
    $request_factory = new jsonapi_example\RequestFactory();

    // if 'auto' mode is set, copy to target server a new saved article
    if ($request_factory->getSettings()['auto'] === "1") {
      $request_factory->addArticle($title, $body);
      \Drupal::messenger()->addStatus(
        t('This article has been copied to the JSON API target server.', [], ['langcode' => 'en'])
      );
    }
  }
}
