<?php


namespace Drupal\jsonapi_example\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Uuid\Uuid;
use Drupal\jsonapi_example\RequestFactory;
use Drupal\jsonapi_example\JsonApiExampleTrait;
use Drupal\Component\Utility\Html;

/**
 * Form to test reading of an distant article on a JSON API server
 *
 * @package Drupal\jsonapi_example
 */
class EditArticleForm extends FormBase {

  use JsonApiExampleTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return SettingsForm::MODULE_NAME . '_modification_article';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['uuid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Edit article with UUID:'),
      '#placeholder' => '6317955f-d8e5-447e-b006-dd959c68b3a3',
      '#size' => SettingsForm::INPUT_MAXSIZE,
    ];
    $form['titre'] = [
      '#type' => 'textfield',
      '#title' => $this->t('New title for the article'),
      '#placeholder' => $this->t('Title', [], ['context' => 'New title of the article (placeholder string)']),
      '#size' => SettingsForm::INPUT_MAXSIZE,
      '#maxlength' => SettingsForm::INPUT_MAXLENGTH,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Modify title of the article'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if (!Uuid::isValid($form_state->getValue('uuid'))) {
      $form_state->setErrorByName('uuid', $this->t('This is not a good format for a UUID'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->getSettings();
    $uuid = Html::escape($form_state->getValue('uuid'), 'plain_text');
    $titre = Html::escape($form_state->getValue('titre'), 'plain_text');

    // Information message on the current action
    $message = $this->t(
      "<p>Edit article title</br> on source</br><strong>@source</strong></p>",
      ['@source' => $config[SettingsForm::SETTINGS_CONN]], ['langcode' => 'en']
    );

    // prepare and launch the request
    $requete = new RequestFactory();
    $message .= $requete->editArticle($uuid, $titre);

    // Display the returned message
    $rendered_message = \Drupal\Core\Render\Markup::create($message);
    \Drupal::messenger()->addStatus($rendered_message);
  }
}
