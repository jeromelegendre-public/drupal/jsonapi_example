<?php


namespace Drupal\jsonapi_example\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Uuid\Uuid;
use Drupal\jsonapi_example\RequestFactory;
use Drupal\jsonapi_example\JsonApiExampleTrait;
use Drupal\Component\Utility\Html;

/**
 * Form to delete a content on target JSON API server
 *
 * @package Drupal\jsonapi_example
 */
class DeleteArticleForm extends FormBase {

  use JsonApiExampleTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return SettingsForm::MODULE_NAME . '_suppression_article';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['uuid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delete content with UUID:'),
      '#placeholder' => '6317955f-d8e5-447e-b006-dd959c68b3a3',
      '#size' => SettingsForm::INPUT_MAXSIZE,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete this content'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if (!Uuid::isValid($form_state->getValue('uuid'))) {
      $form_state->setErrorByName('uuid', $this->t('This UUID is invalid'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->getSettings();
    $uuid = Html::escape($form_state->getValue('uuid'), 'plain_text');

    // Information message
    $message = $this->t(
      "<p>Deleting a content</br> on target</br><strong>@source</strong></p>",
      ['@source' => $config[SettingsForm::SETTINGS_CONN]]
    );

    // Prepare and launch request execution
    $requete = new RequestFactory();
    $message .= $requete->deleteArticle($uuid);

    // Display request result
    $rendered_message = \Drupal\Core\Render\Markup::create($message);
    \Drupal::messenger()->addStatus($rendered_message);
  }
}
