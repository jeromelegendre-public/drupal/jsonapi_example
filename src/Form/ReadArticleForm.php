<?php


namespace Drupal\jsonapi_example\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Uuid\Uuid;
use Drupal\jsonapi_example\RequestFactory;
use Drupal\jsonapi_example\JsonApiExampleTrait;


/**
 * Form for reading an article from  JSON API server
 *
 * @package Drupal\jsonapi_example
 */
class ReadArticleForm extends FormBase {

  use JsonApiExampleTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return SettingsForm::MODULE_NAME . '_lecture_article';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['uuid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Read article with UUID:'),
      '#placeholder' => '6317955f-d8e5-447e-b006-dd959c68b3a3',
      '#size' => SettingsForm::INPUT_MAXSIZE,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Read article'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if (!Uuid::isValid($form_state->getValue('uuid'))) {
      $form_state->setErrorByName('uuid', $this->t('This is not a valid UUID format'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->getSettings();
    $uuid = $form_state->getValue('uuid');

    // Information message for this action
    $message = $this->t("<p>Get article with UUID </br><strong>@uuid</strong></br> on source</br><strong>@source</strong></p>",
      [
        '@uuid' => $uuid,
        '@source' => $config[SettingsForm::SETTINGS_CONN],
      ]);

    // Prepare client and launch request execution
    $requete = new RequestFactory();
    $message .= $requete->getArticle($uuid);

    // Display returned message
    $rendered_message = \Drupal\Core\Render\Markup::create($message);
    \Drupal::messenger()->addStatus($rendered_message);
  }
}
